# Demos Commander

<a href="https://www.buymeacoffee.com/mezantrop" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/default-orange.png" alt="Buy Me A Coffee" height="41" width="174"></a>

## An attempt to revive the famous Deco - Demos Commander by Serge Vakulenko

### Because gems must shine forever!

* [deco39](deco39) - Original sources fetched from https://sourceforge.net/projects/deco
* [deco39.1](deco39.1) - **Current state. Compile this one!**
* [Cumulative patch](deco.patch) - All changes in a single file *deco39* -> *deco39.1*

![Deco](deco.png)

#### Compilation

In the most cases you need to grab the latest snapshot and compile:

* [Download](https://gitlab.com/mezantrop/deco/-/archive/master/deco-master.tar.gz) the tarball
* `$ tar zxvf deco-master.tar.gz`
* `$ cd deco-master/deco39.1`
* `$ ./configure && ./make && ./make install`

Or, if you want to follow the oldschool way, use the [Cumulative patch](deco.patch)
to upgrade *deco39* to the latest version yourself. For example:

* [Download](https://gitlab.com/mezantrop/deco/-/archive/master/deco-master.tar.gz) the tarball
* `$ tar zxvf deco-master.tar.gz`
* `$ cd deco-master && cp -r deco39 deco39.2 && cd deco39.2`
* `$ patch < ../deco.patch`
* `$ ./configure && ./make && ./make install`

Here *deco39.2* directory contains the latest snapshot of *deco39.1*

#### TODO

* Fix output on current Ubuntu Linux

#### Changelog

Date       | Changes
---------- | -------------------------------------------------------------------
2020.05.22 | *FreeBSD* patches applied
2020.05.23 | The most of the warnings when compiling under *FreeBSD 12.x* resolved
2020.05.25 | All compilation warnings on FreeBSD cleared; First successful build under *Linux (Ubuntu)*
2020.05.27 | All compilation warnings under *Linux (Ubuntu)* cleared; *patch.sh* updated
2020.06.01 | Broken *getcwdname()* under *macOS* and *Linux* replaced with *getcwd()*
2020.06.02 | *basename()* issue fixed. Thanks *Edwin Concepción* and *Fernando Toledo*: https://gitlab.com/mezantrop/deco/-/issues/1
2020.06.03 | *OpenWrt* first time successful build. Patches added out of stages in [OpenWrt](OpenWrt)
2020.06.04 | The first successful build on *AIX*
2020.06.07 | The *configure* script generated with *Autoconf 2.69*
2020.06.09 | *AC_CANONICAL_HOST()* in *configure*; Cumulative patch completely replaces patch-stages
2020.07.11 | *deco.patch* updated to include *config.sub* and *config.guess*
2022.12.10 | *configure*, *Makefile* updated to set *-fcommon* for gcc > 10
2022.12.11 | Various memory management issues fixed

Don't hesitate to contact me: Mikhail Zakharov <zmey20000@yahoo.com>
